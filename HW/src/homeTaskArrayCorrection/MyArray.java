package homeTaskArrayCorrection;

class MyArray {
    private String[] myArray = new String[3];

    int size() {
        return myArray.length;
    }
    void addElem(String addEl) {
        if (myArray[size() - 1] != null) {
            String[] cloneArray = new String[size() * 2];
            for (int j = 0; j < size(); j++) {
                cloneArray[j] = myArray[j];
            }
            cloneArray[size()] = addEl;
            myArray = cloneArray;
        } else {
            for (int i = 0; i < size(); i++) {
                if (myArray[i] == null) {
                    myArray[i] = addEl;
                    break;
                }
            }
        }
    }

    String getInd(int index) {
        if (index < size()) {
            return myArray[index];
        } else {
            return "Incorrect index";
        }
    }

    void removeString(String str) {
        int index = size();
        for (int i = 0; i < size(); i++) {
            if (myArray[i].equals(str)) {
                index = i;
                break;
            }
        }
        if (index != size()) {
            for (int i = 0; i < size() - 1; i++) {
                if (i >= index) {
                    myArray[i] = myArray[i + 1];
                }
            }
            myArray[size() - 1] = null;
        } else {
            System.out.println("No such string value");
        }
    }

    void removeStrByInd(int index) {


            if ((index < size())&&(myArray[index]!=null)){
            for (int i = 0; i < size() - 1; i++) {
                if (i >= index) {
                    myArray[i] = myArray[i + 1];

                }
            }

            myArray[size() - 1] = null;


        } else {
            System.out.println("Index is out of the range!");
        }
    }

    void outputArray() {
        String outArr = " array :";
        for (int i = 0; i < size(); i++) {
            outArr += myArray[i];
        }
        System.out.println(outArr);
    }

}
