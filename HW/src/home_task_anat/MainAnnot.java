package home_task_anat;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class MainAnnot {
        public static void main(String[] args) throws InvocationTargetException, IllegalAccessException {
            
            MyClass my=new MyClass();
            Method[] met=my.getClass().getDeclaredMethods();
            for (Method s:met) {
                if(s.isAnnotationPresent(MyAnnotat.class)){
                    s.invoke(my);
                }
            }

        }

}


