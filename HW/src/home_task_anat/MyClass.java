package home_task_anat;

class MyClass {
    public void doOne() {
        System.out.println("Enter 1st");
    }

    public void doTwo() {
        System.out.println("Enter 2nd");
    }

    @MyAnnotat(string = "Again-again")
    public void doAgain() {
        System.out.println("Enter again");
    }
}
