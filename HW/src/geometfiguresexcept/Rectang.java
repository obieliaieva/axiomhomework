package geometfiguresexcept;

class Rectang extends Fig {
    private float width;
    private float height;


    public void setWidth(float width) {
        if (width <= 0) {
            throw new IllegalArgumentException("Exception: width Rectangle  is less or equal 0!");
        }
        if (width > 0) {

            this.width = width;
        }

    }
    public void setHeight(float height) {
        if (height <= 0) {
            throw new IllegalArgumentException("Exception: height Rectangle is less or equal 0!");
        }
        if (height > 0) {
            this.height = height;
        }

    }
    @Override
    public String getName () {
        return "Rectangle has been chosen! Enter action  (1 or 2) for Rectangle : ";
    }
    @Override
    public float getPerimeter() {

        return (float)(width * 2 + height * 2);
    }
@Override
    public float getArea() {
        return  (float)(width * height);

    }
}
