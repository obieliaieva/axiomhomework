package geometfiguresexcept;


public class Romb extends Fig {

    private float sideR;
    private float hR;
    public void setSideR(float sideR) {
        if (sideR <= 0) {
            throw new IllegalArgumentException("Exception: Side Romb is less or equal 0!");
        }
        if (sideR > 0) {
            this.sideR = sideR;
        }
    }
    public void sethR(float hR) {
        if (hR <= 0) {
            throw new IllegalArgumentException("Exception: Heigh Romb is less or equal 0!");
        }
        if (hR > 0) {
            this.hR = hR;
        }

    }
@Override
    public String getName () { return "Romb! Enter action  (1 or 2) for Romb : ";
    }
@Override
    public float getPerimeter() {
        return  (float)(sideR*sideR) ;
    }
@Override
    public float getArea() {
        return (float)(hR*sideR);
    }

}
