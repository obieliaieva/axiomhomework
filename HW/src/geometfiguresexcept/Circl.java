package geometfiguresexcept;

class Circl extends Fig {

    protected double radius;
    final double PI = 3.1415;

    public void setRadius(double radius) {

        if (radius <= 0) {
            throw new IllegalArgumentException("Exception: Radius is less or equal 0!");
        }
        if (radius > 0) {
            this.radius = radius;
        }

    }

    @Override
    public String getName() {
        return "Circle has been chosen! Enter action  (1 or 2) for Circle : ";
    }

    @Override
    public float getPerimeter() {
        return  (float) (2*PI*radius);
    }

    @Override
    public float getArea() {
        return (float) (Math.pow(radius, 2) * PI);
    }
}
