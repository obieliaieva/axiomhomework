package geometfiguresexcept;



public class Squar extends Fig {
    private float sideSq;


    public void setSideSq(float sideSq) {
        if (sideSq<= 0) {
            throw new IllegalArgumentException("Exception: Side Square is less or equal 0!");
        }
        if (sideSq > 0) {
            this.sideSq= sideSq;
        }

    }
    @Override
    public String getName() {
        return "Square! Enter action (1 or 2) for it :";
    }
@Override
    public float getPerimeter() {

            return (float) (sideSq*sideSq);
        }
@Override
    public float getArea() {
        return (sideSq)*4;
    }
}
