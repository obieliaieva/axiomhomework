package geometfiguresexcept;


class Trian extends Fig {
    private float widthT;
    private float heightT;
    private float x;
    private float y;
    private float z;


    public void setWidthT(float widthT) {
        if (widthT<= 0) {
            throw new IllegalArgumentException("Exception: width Triangle is less or equal 0!");
        }
        if (widthT > 0) {
            this.widthT = widthT;
        }

    }
    public void setHeightT(float heightT) {
        if (heightT<= 0) {
            throw new IllegalArgumentException("Exception: height Triangle is less or equal 0!");
        }
        if (heightT > 0) {
            this.heightT = heightT;
        }
    }

        public void setX(float x) {
            if (x<= 0) {
                throw new IllegalArgumentException("Exception: Side X Triangle is less or equal 0!");
            }
            if(x>0) {
                this.x = x;
            }
        }

        public void setY(float y) {
            if (y<= 0) {
                throw new IllegalArgumentException("Exception: Side Y Triangle is less or equal 0!");
            }
            if(y>0) {
                this.y = y;
            }
        }

        public void setZ(float z) {
            if (z<= 0) {
                throw new IllegalArgumentException("Exception: Side Z Triangle is less or equal 0!");
            }
            if(z>0) {
                this.z = z;
            }
            }


        @Override
    public String getName () {
        return "Triangle has been chosen! Enter action  (1 or 2) for Triangle : ";
    }
@Override
    public float getPerimeter(){
        return x+y+z;
    }
@Override
    public float getArea(){
        return (float) (0.5*widthT * heightT);
    }
}
