package geometfiguresexcept;

abstract class Fig {

    public abstract String getName ();
    public abstract float getPerimeter();
    public abstract float getArea();

}

