package abstactclass;
class Rectang extends Fig {
    private float width;
    private float height;


    public void setWidth(float width) {
        if (width > 0) {
            this.width = width;
        } else {
            System.out.println("negative value is not valid\nWidth=0");
        }

    }
    public void setHeight(float height) {
        if (height > 0) {
            this.height = height;
        } else {
            System.out.println("negative value is not valid\nHeight=0");
        }

    }
    @Override
    public String getName () {
        return "Rectangle has been chosen! Enter action  (1 or 2) for Rectangle : ";
    }
    @Override
    public float getPerimeter() {

        return (float)(width * 2 + height * 2);
    }
@Override
    public float getArea() {
        return  (float)(width * height);

    }
}
