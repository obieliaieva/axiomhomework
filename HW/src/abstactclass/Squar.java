package abstactclass;

public class Squar extends Fig {
    private float sideSq;


    public void setSideSq(float sideSq) {
        if (sideSq > 0) {
            this.sideSq= sideSq;
        } else {
            System.out.println("negative value is not valid\nWidth=0");
        }

    }
    @Override
    public String getName() {
        return "Square! Enter action (1 or 2) for it :";
    }
@Override
    public float getPerimeter() {

            return (float) (sideSq*sideSq);
        }
@Override
    public float getArea() {
        return (sideSq)*4;
    }
}
