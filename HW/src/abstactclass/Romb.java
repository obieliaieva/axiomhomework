package abstactclass;


public class Romb extends Fig {

    private float sideR;
    private float hR;
    public void setSideR(float sideR) {
        if (sideR > 0) {
            this.sideR = sideR;
        } else {
            System.out.println("negative value is not valid\nSideRomb=0");
        }

    }
    public void sethR(float hR) {
        if (hR > 0) {
            this.hR = hR;
        } else {
            System.out.println("negative value is not valid\nHeighRomb=0");
        }

    }
@Override
    public String getName () { return "Romb! Enter action  (1 or 2) for Romb : ";
    }
@Override
    public float getPerimeter() {
        return  (float)(sideR*sideR) ;
    }
@Override
    public float getArea() {
        return (float)(hR*sideR);
    }

}
