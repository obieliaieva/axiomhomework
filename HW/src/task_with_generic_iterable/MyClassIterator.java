package task_with_generic_iterable;

import java.util.Iterator;


class MyClassIterator<T> implements Iterator<T> {
    private int index=0;
    private T[] addArray;

    MyClassIterator(T[] addArray){
        this.addArray=addArray;
    }

    @Override
    public boolean hasNext() {
        return addArray.length - 1 >= index;
    }

    @Override
    public T next() {
        T val=addArray[index];
        index++;
        return val;
    }
}

