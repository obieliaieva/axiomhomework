package task_with_generic_iterable;

import java.util.Arrays;
import java.util.Iterator;

public class MyArray <T> implements Iterable<T> {
    private T[] myArray = (T[]) new Object[3];

        int getSize () {
            return myArray.length;
        }
        void addElem (T addEl){
            if (myArray[myArray.length - 1] != null) {
                T[] cloneArray = (T[]) new Object[myArray.length * 2];
                System.arraycopy(myArray, 0, cloneArray, 0, myArray.length);
                cloneArray[myArray.length] = addEl;
                myArray = cloneArray;
            } else {
                for (int i = 0; i < myArray.length; i++) {
                    if (myArray[i] == null) {
                        myArray[i] = addEl;
                        break;
                    }
                }
            }
        }

    T getInd (int index){
        if (index <= myArray.length&&(index>=0)) {
            return myArray[index];
        } else
            try {
                throw new IllegalArgumentException("Index out of range");
            } catch (IllegalArgumentException e) {
                System.out.println("Incorrect index");
            }
        return null;
}

        void removeString (T str){
            int index = myArray.length;
            for (int i = 0; i < myArray.length; i++) {
                if (!(myArray[i] ==null)){
                if (myArray[i].equals(str)) {
                    index = i;
                    break;
                }
            }
            }
            if (index != myArray.length) {
                for (int i = 0; i < myArray.length - 1; i++) {
                    if (i >= index) {
                        myArray[i] = myArray[i + 1];
                    }
                }
                myArray[myArray.length - 1] = null;
            } else {
                System.out.println("No such string value");
            }
        }

        void removeStrByInd ( int index){

            if ((index < myArray.length) && (myArray[index] != null)) {
                for (int i = 0; i < myArray.length - 1; i++) {
                    if (i >= index) {
                        myArray[i] = myArray[i + 1];
                    }
                }
                myArray[myArray.length - 1] = null;

            } else {
                System.out.println("Index is out of the range!");
            }
        }
        void outputArray () {
            String outArr = "Array :";
            for (T s : myArray) {
                outArr += s;
            }
            System.out.println(Arrays.toString(myArray));
        }

    @Override
    public Iterator<T> iterator() {
        return new MyClassIterator<T>(myArray);
    }
}

