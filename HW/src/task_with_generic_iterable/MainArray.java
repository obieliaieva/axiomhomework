package task_with_generic_iterable;

public class MainArray {
    public static void main(String[] args) throws NullPointerException {

        MyArray<String> myArray = new MyArray<>();

        myArray.addElem("a");
        myArray.addElem("b");
        myArray.removeString("c");
        myArray.removeStrByInd(6);
        myArray.getInd(7);
        myArray.getInd(-1);
        myArray.getInd(0);
        myArray.outputArray();
        myArray.addElem("c");
        myArray.outputArray();
        myArray.removeString("c");
        myArray.outputArray();
        myArray.removeStrByInd(1);
        myArray.addElem("e");
        myArray.addElem("f");
        myArray.addElem("g");
        myArray.outputArray();
        for (String s : myArray
        ) {
            System.out.println(s);

        }

    }
}


        



