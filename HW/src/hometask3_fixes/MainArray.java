package hometask3_fixes;

public class MainArray {
    public static void main(String[] args) {
        MyArray myArray = new MyArray();
        System.out.println("Size for 1st Array " + myArray.size());
        myArray.addElem("1");
        myArray.addElem("2");
        myArray.addElem("3");
        myArray.outputArray();
        myArray.addElem("5");
        System.out.println("Size after changes " + myArray.size());
        myArray.outputArray();
        System.out.println(myArray.getInd(2));
        myArray.removeString("1");
        myArray.outputArray();
        System.out.println("Size after changes " + myArray.size());
        myArray.removeStrByInd(5);
        myArray.outputArray();



    }
}
