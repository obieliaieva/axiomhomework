package home_task_generic_without_iter;

import java.util.Arrays;
import java.util.Iterator;

public class MyArray <T> implements Iterable{

        @Override
        public Iterator iterator() {
            return null;
        }

    private T[] myArray = (T[]) new Object[3];


    int getSize() {
        return myArray.length;
    }
    void addElem(T addEl) {
        if (myArray[myArray.length - 1] != null) {
            T[] cloneArray = (T[])new Object[myArray.length*2];
            System.arraycopy(myArray, 0, cloneArray, 0, myArray.length);
            cloneArray[myArray.length] = addEl;
            myArray = cloneArray;
        } else {
            for (int i = 0; i < myArray.length; i++) {
                if (myArray[i] == null) {
                    myArray[i] = addEl;
                    break;
                }
            }
        }
    }

    T getInd(int index) {
        if (index < myArray.length) {
            return myArray[index];
        } else {
            return (T) "Incorrect index";
        }
    }

    void removeString(T str) {
        int index = myArray.length;
        for (int i = 0; i < myArray.length; i++) {
            if (myArray[i].equals(str)) {
                index = i;
                break;
            }
        }
        if (index != myArray.length) {
            for (int i = 0; i < myArray.length - 1; i++) {
                if (i >= index) {
                    myArray[i] = myArray[i + 1];
                }
            }
            myArray[myArray.length - 1] = null;
        } else {
            System.out.println("No such string value");
        }
    }

    void removeStrByInd(int index) {

        if ((index < myArray.length)&&(myArray[index]!=null)){
            for (int i = 0; i < myArray.length - 1; i++) {
                if (i >= index) {
                    myArray[i] = myArray[i + 1];
                }
            }
            myArray[myArray.length - 1] = null;

        } else {
            System.out.println("Index is out of the range!");
        }
    }
    void outputArray() {
        String outArr = "Array :";
        for (T s : myArray) {
            outArr += s;
        }
        System.out.println(Arrays.toString(myArray));
    }



}
