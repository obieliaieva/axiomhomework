package home_task_generic_without_iter;

public class MainArray {
    public static void main(String[] args) {
        MyArray<Object> myArray = new MyArray<>();

        System.out.println("Size for 1st Array: " + myArray.getSize());
        myArray.addElem("1");
        myArray.addElem("2");
        myArray.addElem("0");
        myArray.outputArray();
        myArray.addElem("3");
        myArray.outputArray();
        System.out.println("Size after changes: " + myArray.getSize());
        System.out.println("Index: " + myArray.getInd(6));
        myArray.addElem("5");
        myArray.addElem("51");
        myArray.addElem("52");
        System.out.println("Size after changes: " + myArray.getSize());
        myArray.outputArray();
        System.out.println(myArray.getInd(5));
        myArray.removeString("1");
        System.out.println("Size after changes: " + myArray.getSize());
        myArray.outputArray();

        myArray.removeStrByInd(0);
        System.out.println("Size after changes: " + myArray.getSize());
        myArray.outputArray();

        myArray.addElem("3");
        System.out.println("Size after changes: " + myArray.getSize());
        myArray.outputArray();
        myArray.removeStrByInd(0);

        System.out.println("Size after changes: " + myArray.getSize());
        myArray.outputArray();
    }

}
