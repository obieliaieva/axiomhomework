package hometask4;

public class Romb extends Figures {
    protected double high;
    protected double side1;
    Romb(){}
    Romb (double high, double side1) {
        this.high = high;
        this.side1 = side1;
    }

    @Override
    public String getName () {
        return super.getName() + "Romb! Enter 1 0r 2 for Rectangle action: ";
    }

    @Override
    public double getSquare (double high, double side1) {
        return (high*side1) ;
    }
    @Override
    public final double getPerimeter (double side1) {
        return (side1) *4;
    }

}
