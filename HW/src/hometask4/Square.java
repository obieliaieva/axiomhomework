package hometask4;

public class Square extends Figures {
    protected double side1;


    Square() {
    }

    Square(double side1) {
        this.side1 = side1;
    }

    @Override
    public String getName() {
        return super.getName() + "Square! Enter 1 or 2  for action :";
    }

    @Override
    public double getSquare(double side1) {
        return (double) (side1*side1);
    }

    @Override
    public double getPerimeter(double side1) {
        return (side1)*4;
    }
}


