package hometask4;

import java.util.Scanner;

public class MainForFigures {
    public static void main(String[] args) {
        Triangle triangle = new Triangle();
        Rectangle rectangle = new Rectangle();
        Square square = new Square();
        Romb romb = new Romb();
        Circle circle = new Circle();

        System.out.println("Enter number from 1 to 5 to chose Figure");
        Scanner figure = new Scanner(System.in);
        int forFig = figure.nextInt();
        switch (forFig) {
            case 1:
                System.out.println(triangle.getName());
                Scanner act = new Scanner(System.in);
                int actForTr1 = act.nextInt();
                if (actForTr1 == 1) {
                    System.out.println("Enter 1st number for getting Perimeter");
                    Scanner tr1 = new Scanner(System.in);
                    double side1 = tr1.nextDouble();
                    System.out.println("Enter 2nd number for getting Perimeter");
                    Scanner tr2 = new Scanner(System.in);
                   double side2 = tr2.nextDouble();
                    System.out.println("Enter 3rd number for getting Perimeter");
                    Scanner tr3 = new Scanner(System.in);
                    double side3 = tr3.nextDouble();
                    System.out.println("Perimeter of Triangle: "+triangle.getPerimeter(side1, side2,side3));
                }
                if (actForTr1 == 2) {
                    System.out.println("Enter 1st number for getting Square");
                    Scanner tr1 = new Scanner(System.in);
                    double hight = tr1.nextDouble();
                    System.out.println("Enter 2nd number for getting Square");
                    Scanner tr2 = new Scanner(System.in);
                    double wide = tr2.nextDouble();
                    System.out.println("Square of Triangle: "+triangle.getSquare(hight,wide));
                }
                break;
            case 2:
                System.out.println(rectangle.getName());
                Scanner act2 = new Scanner(System.in);
                int actForRect = act2.nextInt();
                if (actForRect == 1) {
                    System.out.println("Enter Rectangle side for getting Perimeter");
                    Scanner tr1 = new Scanner(System.in);
                    double side1 = tr1.nextDouble();
                    System.out.println("Enter 2nd number for for getting Perimeter for Rectangle");
                    Scanner tr2 = new Scanner(System.in);
                    double side2 = tr2.nextDouble();
                    System.out.println("Perimeter of Rectangle: "+rectangle.getPerimeter(side1, side2));
                }
                if (actForRect == 2) {
                    System.out.println("Enter 1st number for getting Square of Rectangle");
                    Scanner tr1 = new Scanner(System.in);
                    double side1 = tr1.nextDouble();
                    System.out.println("Enter 2nd number for getting Square of Rectangle");
                    Scanner tr2 = new Scanner(System.in);
                    double side2 = tr2.nextDouble();
                    System.out.println("Square of Rectangle: "+rectangle.getSquare(side1,side2));
                }

                break;
            case 3:
                System.out.println(square.getName());
                Scanner act3 = new Scanner(System.in);
                int actForSquare = act3.nextInt();
                if (actForSquare == 1) {
                    System.out.println("Enter Square side for getting Perimeter");
                    Scanner tr1 = new Scanner(System.in);
                    double side1 = tr1.nextDouble();
                    System.out.println("Perimeter of Square: "+square.getPerimeter(side1));
                }
                if (actForSquare == 2) {
                    System.out.println("Enter Square side for getting Square");
                    Scanner tr1 = new Scanner(System.in);
                    double side1 = tr1.nextDouble();
                    System.out.println("Square of Square: "+square.getSquare(side1));
                }
                break;
            case 4:
                System.out.println(romb.getName());
                Scanner actRomb = new Scanner(System.in);
                int actForRomb = actRomb.nextInt();
                if (actForRomb == 1) {
                    System.out.println("Enter Romb side for getting Perimeter");
                    Scanner tr1 = new Scanner(System.in);
                    double side1 = tr1.nextDouble();
                    System.out.println("Perimeter of Romb: "+ romb.getPerimeter(side1));
                }
                if (actForRomb == 2) {
                    System.out.println("Enter Romb heigh for getting Square");
                    Scanner tr1 = new Scanner(System.in);
                    double high = tr1.nextDouble();
                    System.out.println("Enter Romb side for getting Square");
                    Scanner tr2 = new Scanner(System.in);
                    double side1 = tr2.nextDouble();
                    System.out.println("Square of Romb: " + romb.getSquare(side1, high));
                }
            case 5:
                System.out.println(circle.getName());
                Scanner actCircle = new Scanner(System.in);
                int actCirc = actCircle.nextInt();
                if (actCirc == 1) {
                    System.out.println("Enter Circle radius for getting Circle");
                    Scanner tr1 = new Scanner(System.in);
                    double side1 = tr1.nextDouble();
                    System.out.println("Circle of Circle: "+ circle.getPerimeter(side1));
                }
                if (actCirc == 2) {
                    System.out.println("Enter Circle radius for getting Square");
                    Scanner tr1 = new Scanner(System.in);
                    double radius = tr1.nextDouble();
                    System.out.println("Square of Circle: " + circle.getSquare(radius));
                }


            default:
                System.out.println("Incorrect value");
        }


    }
}
