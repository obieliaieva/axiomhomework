package hometask4;

public class Figures {
private double side1;
private double side2;
private double side3;
    Figures (){
    }
    Figures(double side1, double side2) {
        setX(side1);
        setY(side2);
    }
    Figures(double side1, double side2,double side3) {
        setX(side1);
        setY(side2);
        setZ(side3);
    }

    public double getX() {
        return side1;
    }

    public void setX(double side1) {
        if (side1< 0) {
            System.out.println("Side1 не может быть отрицательным");
        }
        this.side1 = side1;
    }

    public double getY() {
        return side2;
    }

    public void setY(double side2) {
        if (side2 < 0) {
            System.out.println("Side2 не может быть отрицательным");
        }
        this.side2 = side2;

    }
    public double getZ() {
        return side3;
    }
    public void setZ(double side3) {
        if (side3 < 0) {
            System.out.println("Side2 не может быть отрицательным");
        }
        this.side3 = side3;

    }
    public String getName () {
        return "Figure ";
    }
    public double getSquare (double side1, double side2){
            return (side1*side2) ;
    }
    public double getSquare (double side1){
        return (side1) ;
    }
    public double getPerimeter (double side1, double side2){
        return (side1+side2);
    };
    public double getPerimeter (double side1){
        return (side1);
    };
    public double getPerimeter (double side1, double side2,double side3){
        return (side1+side2+side3);
    }

}
