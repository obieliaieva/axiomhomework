package hometask4;

public  class Triangle  extends Figures{

    protected double hight;
    protected double wide;
    protected double side1;
    protected double side2;
    protected double side3;
    Triangle (){}

    Triangle (double hight, double wide) {
        this.hight = hight;
        this.wide = wide;
    }
    Triangle (double side1, double side2, double side3) {
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
    }

    @Override
    public String getName () {
        return super.getName() + "You`ve chosen Triangle! Enter 1 or 2 for action";
    }

    @Override
    public double getSquare (double hight, double wide) {
        return (0.5 * hight * wide);
    }
    @Override
     public double getPerimeter (double side1, double side2, double side3) {
        return (side1+side2+side3);
    }

}
