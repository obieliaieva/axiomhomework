package hometask4;

public class Circle extends Figures {

    protected double radius;
    final double PI=3.1415;

    Circle() {
    }

    Circle(double r) {
        radius = r;
    }

    @Override
    public String getName() {
        return super.getName() + "Circle! Enter 1 or 2 for action :";
    }

    @Override
    public double getSquare(double radius) {
        return (Math.pow(radius, 2) * PI);
    }

    @Override
    public double getPerimeter(double radius) {
        return 2*PI*radius;
    }

}

