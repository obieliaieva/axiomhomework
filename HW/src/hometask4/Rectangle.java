package hometask4;

public class Rectangle extends Figures {
    protected double side1;
    protected double side2;
    Rectangle(){}
    Rectangle (double side1, double side2) {
        this.side1 = side1;
        this.side2 = side2;
    }

    @Override
    public String getName () {
        return super.getName() + "Rectangle! Enter 1 or 2 for action : ";
    }

    @Override
    public  double getSquare (double side1, double side2) {
        return (side1*side2) ;
    }
    @Override
    public  double getPerimeter (double side1, double side2) {
        return (double)(side1+side2) *2;
    }


}
