package hometask3;

public class MyArrayTask {

    private int addNum = 20;
    private String[] myArray = new String[3];
    private String[] myArrayClone = new String[0];
    private String[] myArrayForDel = new String[0];

    public String[] getMyArray() {
        return myArray;
    }


    public int size() {
        int numElemArrayOne = 0;
        for (int i = 0; i < myArray.length; i++) {
            if (myArray[i] != null) {
                numElemArrayOne++;
            }
        }
        return numElemArrayOne;
    }

    public int sizeClone() {
        int numElemArrayTwo = 0;
        for (int i = 0; i < myArrayClone.length; i++) {
            if (myArrayClone[i] != null) {
                numElemArrayTwo++;
            }
        }
        return numElemArrayTwo;
    }


    public void addElem() {

        for (int i = 0; i < addNum; i++) {
            if (i < myArray.length) {
                myArray[i] = "qwe" + i;
                System.out.println("Добавляем эллемент в первый массив - " + myArray[i]);
            } else if (i >= myArray.length) {
                myArrayClone = new String[myArray.length * 2];
                for (int j = 0; j < myArray.length * 2; j++) {
                    myArrayClone[j] = "qwe" + j;
                    i = addNum;
                    System.out.println("Добавляем эллемент из первого во второй массив - " + myArrayClone[j]);
                }
            }
        }
    }

    public String getInd(String[] name, int ind) {
        String retValue = "Incorrect index";


        for (int i = 0; i < name.length; i++) {
            if (i == ind) {
                retValue = name[i];
            }
        }
        return retValue;
    }

    public void removeStr(int index) {

        myArrayForDel = new String[myArray.length - 1];
        int count = 0;
        for (int i = 0; i < myArray.length; i++) {
            if (i != index) {
                myArrayForDel[count] = myArray[i];
                count++;
            }
        }
        myArray = myArrayForDel;
    }

    public void removeString(String string) {

        myArrayForDel = new String[myArray.length];
        int count = 0;
        for (int i = 0; i < myArray.length; i++) {
            if (!myArray[i].equals(string)) {
                myArrayForDel[count] = myArray[i];
                count++;
            }
        }
        myArray = myArrayForDel;

    }

    public void removeStrClone(int index) {

        myArrayForDel = new String[myArrayClone.length - 1];
        int count = 0;
        for (int i = 0; i < myArrayClone.length; i++) {
            if (i != index) {
                myArrayForDel[count] = myArrayClone[i];
                count++;
            }
        }
        myArrayClone = myArrayForDel;
    }

    public String[] trunkateArray() {
        String[] clearArray = new String[0];
        myArray = clearArray;
        return myArray;
    }

    void outputArray() {
        String outArr = " array :";
        for (int i = 0; i < myArray.length; i++) {
            outArr = outArr + myArray[i];
        }
        System.out.println(outArr);
    }

}



