package hometask3;

public class MainMyArray {
    public static void main(String[] args) {
        MyArrayTask myArrayTask = new MyArrayTask();

        myArrayTask.addElem();

        System.out.println("Size for 1st Array " + myArrayTask.size());
        System.out.println("Size for Clone Array " + myArrayTask.sizeClone());

        myArrayTask.removeStr(0);
        System.out.println("Deleted from 2st");


        System.out.println("Size for 1st Array " + myArrayTask.size());
        System.out.println("String for chosen index is " + myArrayTask.getInd(myArrayTask.getMyArray(), 0));
        myArrayTask.removeStrClone(3);
        System.out.println("Deleted from Clone");

        System.out.println("Size for Clone Array " + myArrayTask.sizeClone());

        myArrayTask.addElem();
        System.out.println("Size for 1st Array " + myArrayTask.size());
        System.out.println("Size for Clone Array " + myArrayTask.sizeClone());
        myArrayTask.outputArray();
        myArrayTask.removeString("qwe2");
        myArrayTask.outputArray();
        myArrayTask.trunkateArray();
        myArrayTask.outputArray();

    }
}